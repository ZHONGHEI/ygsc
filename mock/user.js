import {
	baseUrl
} from '@/utils/http.js'
import {
	getQueryString
} from '@/utils/common.js'
// 引入mockjs
const Mock = require('mockjs')
// 获取 mock.Random 对象
const Random = Mock.Random
// Mock.mock( url, post/get , 返回的数据)；

/** 测试 */
// Mock.mock(RegExp(baseUrl + '/api/test'+'.*'), 'get', (options) => {
// 	console.log('ops111111', options);
// 	let id = getQueryString(options.url, 'id')
// 	console.log('id', id);
// 	return Mock.mock({
// 		"success": true,
// 		"message": "操作成功！",
// 		"code": 0,
// 		"result|10": [{ // 生成10条数据
// 			"id|+1": 1, // id递增1
// 			"roleName|1": ["测试角色1", "测试角色2", "测试角色3", "测试角色4", "测试角色5"], // 数组内随机一个值
// 			"roleCode": "100010",
// 			"description": null,
// 			"createBy": "jeecg",
// 			"createTime": "2019-10-30 19:50:56",
// 			"updateBy": null,
// 			"updateTime": null
// 		}],
// 		"timestamp": 1573456699794
// 	})
// })


/** 登录 */
Mock.mock(baseUrl + '/api/login', 'post', (options) => {
	return Mock.mock({
		"success": true,
		"message": "操作成功！",
		"code": 0,
		'token|32': Random.guid()
	})
})

/** 用户信息*/
Mock.mock(baseUrl + '/api/user/info', 'post', (options) => {
	console.log(options)
	return Mock.mock({
		"success": true,
		"message": "操作成功！",
		"code": 0,
		'data': {
			"id|1-100": 1,
			"nickName": Random.first(),
			"avatarUrl": Random.dataImage(),
			"createTime": Random.date('yyyy-MM-dd hh:mm:ss'),
			"updateBy": null,
			"updateTime": null,
			"coupons|1-100": 1,
			"cards|1-100": 1,
			"bonus|1-100.1-10": 1
		}
	})
})
