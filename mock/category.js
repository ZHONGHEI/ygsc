export const cateList = [{
	id: 1,
	type: "手机通讯",
	cate:['苹果','华为','荣耀','oppo','小米','三星','一加']
}, {
	id: 2,
	type: "手机配件",
	cate:['数据线','充电器','耳机','移动电源']
}, {
	id: 3,
	type: "电脑办公",
	cate:['苹果','华为']
}, {
	id: 4,
	type: "平板电脑",
	cate:['苹果','华为','荣耀']
}, {
	id: 5,
	type: "耳机音响",
	cate:['苹果','华为','beats']
}, {
	id: 6,
	type: "智能手表",
	cate:['智能手表','智能手环','儿童手表']
}, {
	id: 7,
	type: "二手良品",
	cate:['苹果手机','安卓手机']
}]
