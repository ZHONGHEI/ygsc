
export const swriperList = [
	"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1663447069,2684108413&fm=26&gp=0.jpg",
	"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1277442026,3930767648&fm=15&gp=0.jpg",
	"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1277442026,3930767648&fm=15&gp=0.jpg"
]
export const titleList = [
	"https://dss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2321691662,3578580405&fm=26&gp=0.jpg",
	"https://dss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2321691662,3578580405&fm=26&gp=0.jpg",
	"https://dss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2321691662,3578580405&fm=26&gp=0.jpg",
	"https://dss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2321691662,3578580405&fm=26&gp=0.jpg"
]
export const phoneList = [{
		id: 1,
		name: "小米",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 2,
		name: "华为",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 3,
		name: "三星",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 4,
		name: "苹果",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 5,
		name: "oppo",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 6,
		name: "耳机",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 7,
		name: "平板",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	},
	{
		id: 8,
		name: "充电器",
		url: "https://res.vmallres.com/pimages//product/6972453164759/78_78_A50AA22EAE940A8A16C04D6415175495C595576F1BE54900mp.png"
	}
]
export const phoneItemList = [{
		title: "新品首发",
		list: [{
				id: 1,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 2,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 3,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 4,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 5,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 6,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			}
		]
	},
	{
		title: "店长首发",
		list: [{
				id: 1,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 2,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 3,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 4,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 5,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			},
			{
				id: 6,
				imageUrl: "https://img10.360buyimg.com/n7/jfs/t1/144222/10/4116/228035/5f227e06Ec117ffb5/20a6650531e01543.jpg",
				phoneName: "Apple iPhone XS 苹果Xs 手机 深空灰色 4G全网通 64GB ",
				price: "3522.00",
				oldPrice: "0.00"
			}
		]
	},
]
