import Vue from 'vue'
import App from './App'
// 引入组件
// import cuCustom from './colorui/components/cu-custom.vue'
// Vue.component('cu-custom',cuCustom)
// 工具方法
import {
  isEmpty
} from '@/utils/common.js';
require('./mock/mock.js')
Vue.prototype.isEmpty = isEmpty;

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
