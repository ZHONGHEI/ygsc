import {
	url,
	post,
	get
} from '@/utils/http.js';


const hp_user = {
	login(data) {
		return post('/api/login',data);
	},
	user_info(){
		return post('/api/user/info');
	}
}
export default hp_user;