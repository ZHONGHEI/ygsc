export const baseUrl = 'http://localhost:8080';
//带Token请求post
export const post = (url, data) => {
	let token = "";
	uni.getStorage({
		key: 'token',
		success: function(ress) {
			token = ress.data
		}
	});
	//此token是登录成功后后台返回保存在storage中的
	let promise = new Promise(function(resolve, reject) {
		uni.request({
			url: baseUrl + url,
			data: data || {},
			method: 'POST',
			header: {
				'Token': token,
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			dataType: 'json',
			success: ((res) => {
				if (res.statusCode === 403 || res.statusCode === 401) {
					// 错误处理，返回登录页
					uni.reLaunch({
						url: '/pages/index/index'
					})
				} else if (res.statusCode === 200) {
					resolve(res['data'])
				}
			}),
			fail: ((err) => {
				reject(err)
			})

		});
	})
	return promise
};
//带Token请求get
export const get = (url, data) => {
	let token = "";
	uni.getStorage({
		key: 'token',
		success: function(ress) {
			token = ress.data
		}
	});
	//此token是登录成功后后台返回保存在storage中的
	let promise = new Promise(function(resolve, reject) {
		uni.request({
		url: baseUrl + url,
		data: data || {},
		method: 'GET',
		header: {
			'Token': token,
			'X-Requested-With': 'XMLHttpRequest',
			"Accept": "application/json",
			"Content-Type": "application/json; charset=UTF-8"
		},
		dataType: 'json',
		success: ((res) => {
			if (res.statusCode === 403 || res.statusCode === 401) {
				// 错误处理，返回登录页
				uni.reLaunch({
					url: '/pages/index/index'
				})
			} else if (res.statusCode === 200) {
				resolve(res['data'])
			}
		}),
		fail: ((err) => {
			reject(err)
		})
	});
	})
	return promise;
};
