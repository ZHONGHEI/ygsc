export function baseUrl(url) {
	if (isEmpty(url)) {
		return null;
	}
	if (url.indexOf('https://') >= 0 || url.indexOf('http://') >= 0) {
		return url;
	}
	return process.env.BASE_API + url;
}


/**
 * 判断是否为空
 * @param object 判断值
 * */
export function isEmpty(object) {
	if (object === null || object === undefined || object === '') {
		return true;
	} else if (Object.prototype.toString.call(object) === '[object Array]' && object.length === 0) {
		return true;
	} else if (Object.prototype.toString.call(object) === '[object Object]' && JSON.stringify(object) === '{}') {
		return true;
	} else {
		return false;
	}
}
/**
 * 提取url后的参数
 * @param url  name需要提取的字段
 * */
export function getQueryString(url, name) {
	let urlObj = new URL(url)
	let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	let r = urlObj.search.substr(1).match(reg);
	if (r != null) return unescape(r[2]);
	return null;
}

/**
 * 复制对象
 * @param object 值
 * */
export function copy(object) {
	return JSON.parse(JSON.stringify(object));
}

/**
 * 数组转字符串，转换成1,2,3,4逗号间隔
 * @param array 数组
 * @param keyName 数组列键名
 * @param split 间隔符号
 * */
export function arrayToStringSplit(array, keyName, split) {
	if (array == null || array === '') {
		return '';
	}
	let str = '';
	for (let i = 0; i < array.length; i++) {
		const val = array[i];
		if (typeof val === 'object') {
			str = str + val[keyName] + split;
		} else {
			str = str + val + split;
		}
	}
	return str.substr(0, str.length - split.length);
}

/**
 * 获取数组某个字段的集合
 * @param array 数组
 * @param field 字段名称
 * */
export function arrayToFieldList(array, field) {
	if (array === null || array === undefined) {
		return [];
	}
	return array.map(function(v) {
		return v[field];
	});
}

/**
 * 数组去重
 * @param array 数组
 * @param field 字段名称
 * */
export function arrayToUniq(array, isZero) {
	let temp = [];
	const len = array.length;
	for (let i = 0; i < len; i++) {
		for (let j = i + 1; j < len; j++) {
			if (array[i] === array[j]) {
				i++;
				j = i;
			}
		}
		temp.push(array[i]);
	}
	if (isZero) {
		// 去0
		temp = temp.filter(item => item !== 0 && item !== '0');
	}
	return temp;
}

/**
 * 数组按某个字段去重
 * @param array 数组
 * @param field 字段名称
 * */
export function arrayFieldToUniq(array, field) {
	const temp = [];
	const len = array.length;
	for (let i = 0; i < len; i++) {
		for (let j = i + 1; j < len; j++) {
			if (array[i][field] === array[j][field]) {
				i++;
				j = i;
			}
		}
		temp.push(array[i]);
	}
	return temp;
}

/**
 * 格式化日期
 * @param date 日期对象
 * @param fmt 格式化样式
 * */
export function formatDate(date, fmt) {
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
	}
	const o = {
		'M+': date.getMonth() + 1,
		'd+': date.getDate(),
		'h+': date.getHours(),
		'm+': date.getMinutes(),
		's+': date.getSeconds()
	};
	for (const k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			const str = o[k] + '';
			fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : (('00' + str).substr(str.length)));
		}
	}
	return fmt;
}
